const search_bar = '[data-test="desktop_public_search_bar"]'
const search_bar_button = '[data-test="Desktop_search_button"]'
const search_results = '[data-test="public_search_results"]'

class SearchPage {
    static visit () {
      cy.visit(Cypress.config('baseUrl'))
    }

    static assertSearchLoaded () {
      cy.get(search_bar).should('be.visible')
    }

    static inputSearchTerm (term) {
      switch (term) {
        case "Leadership": 
          break
        case "Team":
          break
        case "Management":
      }
      cy.get(search_bar).type(term)
    }

    static clickSearchButton () {
      cy.get(search_bar_button).click()
    }

    static searchResults (term) {
      switch (term) {
        case "Leadership":
          break
        case "Team":
          break
        case "Management":
      }
      cy.get(search_results).contains(term)  
    }
  }

export default SearchPage