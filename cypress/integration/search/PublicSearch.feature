Feature: Search Functionality

    Scenario: Search Term "Flow"
        Given I am on the homepage
        When I enter the search term "Flow"
        And I click the search button
        Then the results display resources related to "Flow" 

    
    Scenario: Search Term "Leadership"
        Given I am on the homepage
        When I enter the search term "Leadership"
        And I click the search button
        Then the results display resources related to "Leadership" 
        
    Scenario: Search Term "Team"    
        Given I am on the homepage
        When I enter the search term "Team"
        And I click the search button
        Then the results display resources related to "Team" 
 
    Scenario: Search Term "Management"    
        Given I am on the homepage
        When I enter the search term "Management"
        And I click the search button
        Then the results display resources related to "Management" 