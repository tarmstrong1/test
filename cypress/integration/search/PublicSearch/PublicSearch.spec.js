/* global Given, Then, When */

import SearchPage from '../../../support/pages/SearchPage'

Given('I am on the homepage', () => {
  SearchPage.visit()
  SearchPage.assertSearchLoaded()
})

When('I enter the search term {string}', term => {
  SearchPage.inputSearchTerm(term)
})

When('I click the search button', () => {
  SearchPage.clickSearchButton()
})

Then('the results display resources related to {string}', results => {
  SearchPage.searchResults(results)
})
