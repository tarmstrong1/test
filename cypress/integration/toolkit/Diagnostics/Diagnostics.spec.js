import DiagnosticsPage from '../../../support/pages/DiagnosticsPage'

/* global Then, When */

When('I access the environment monitoring URL', () => {
  DiagnosticsPage.visit()
  DiagnosticsPage.assertPageLoaded()
})

Then('the environment status should read {string}', status => {
  // return trues
  DiagnosticsPage.assertStatusisOK(status)
})
