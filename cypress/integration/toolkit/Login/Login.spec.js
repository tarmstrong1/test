/* global Given, Then, When */
import MainPage from '../../../support/pages/MainPage'
import LoginPage from '../../../support/pages/LoginPage'
let mainPage = new MainPage()

Given('I access login url', () => {
  LoginPage.visit()
  LoginPage.assertPageLoaded()
})

When('I enter valid credentials', () => {
  LoginPage.typeLogin(Cypress.env('test_user'))
  LoginPage.typePassword(Cypress.env('test_pass'))
})

When('I click the sign in button', () => {
  mainPage = LoginPage.pressLogin()
})

Then('I succesfully log in', () => {
  mainPage.assertPageLoaded()
})
