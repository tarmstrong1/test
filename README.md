1. Run npm install to install all packages
2. Create a Cypress.env.json on the root of the project and ask lucas@devsquad.email for Helix credentials
3. Run npx cypress open to open Cypress's Test Manager
4. Click on any feature to start its testing
